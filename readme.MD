## 简单的生成mysql的gorm模型的垃圾工具，仅供自己娱乐
##
安装:go install gitee.com/hellochuang/gorm-gen@latest
##
使用:gorm-gen -db=biz -host=127.0.0.1 -port=3306 -user=root  -password= -out=models/model.go

## 参数说明

host:主机地址，不填默认127.0.0.1

port:端口，不填默认3306

user:用户，不填默认root

password:密码，不填默认为空

db:数据库名，不填默认test

out:生成模型的输出目录地址，默认models/model.go